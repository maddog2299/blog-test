**For launch app your need install NodeJs v6.9 or higher** 
_It`s better to install babel and webpack globaly_

- After cloning repo please do npm install in root folder

- npm start - for launch dev server 
(after that you can visit localhost:8085 and check the app)

- npm run build - bundle.js for production

- If you running dev sever on localhost your browser could return an error because of cors. For correct working please install Cors Allow control origin extension in Google Chrome.
