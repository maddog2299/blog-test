'use strict';

import moment from 'moment';
export function parseTime (item) {
    let date;
    date = item.created.split(" ")[0].split("/");
    date = date[1]+"/"+date[0]+"/"+date[2];
    date = moment(date);
    let parsedDate = {
        day: date.format("dddd"),
        month: date.format("MMMM"),
        number: date.format("DD"),
        year: date.format("YYYY"),
    }
    return parsedDate;
}