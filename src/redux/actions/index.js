'use strict';

import types from '../constants';
import api from '../../api';
import config from '../../../configs/config';
import {hashHistory} from 'react-router';

export function getArticles (page=1, limit=config.pageLimit) {
    return dispatch => {
        dispatch({type: types.PENDING});
        api.getArticles(page, limit)
        .then(
            response => {
                dispatch({type: types.GET_ARTICLES_SUCESS, payload: response})
                hashHistory.push(`/?page=${page}`)
            },
            error => {
                dispatch({type: types.GET_ARTICLES_FAILED, payload: error})
                throw error
            }
        )
    }
}

export function getArticleByID (id) {
    return dispatch => {
        api.getArticleById(id)
        .then(
            response => {
                dispatch({type: types.GET_SINGLE_SUCCESS, payload: response})
            },
            error => {
                dispatch({type: types.GET_SINGLE_FAILED, payload: error})
                throw error
            }
        )
    }
}

export function postComment (data, id) {
    return dispatch => {
        api.addComment(data, id)
        .then(
            response => {
                dispatch({type: types.GET_SINGLE_SUCCESS, payload: response})
            },
            error => {
                dispatch({type: types.POST_COMMENT_FAILED, payload: error})
                throw error
            }
        )
    }
}

export function replyTo (id, author) {
    let data = {id, author}
    return dispatch => {
        dispatch({type: types.POST_COMMENT_REPLY_TO, payload: data})
    }
}

export function changeContent (id, body) {
    let data = {
        body: body
    };
    return dispatch => {
        api.updateArticle(id, data)
        .then(
            response => {
                dispatch({type: types.GET_SINGLE_SUCCESS, payload: response})
            },
            error => {
                dispatch({type: types.UPDATE_ARTICLE_FAILED, payload: error})
                throw error
            }
        )
    }
}

export function createPost (data) {
    return dispatch => {
        api.createArticle(data)
        .then(
            response => {
                dispatch({type: types.GET_ARTICLES_SUCESS, payload: response})
            },
            error => {
                dispatch({type: types.CREATE_ARTICLE_FAILED, payload: error})
                throw error
            }
        )
    }
}