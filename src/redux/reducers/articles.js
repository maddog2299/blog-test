'use strict';
import types from '../constants';
import {parseTime} from '../../helper';

const initialState = {};
let decr;
export default function articles (state = initialState, action) {
    let payload = action.payload;
    switch (action.type) {
        case types.PENDING:
            return {...state, pending: true}
        case types.GET_ARTICLES_SUCESS:
            const articlesMeta = [];
            payload.articles.map((item)=> {
                (item.metaDescription.length>50)?decr=item.metaDescription.substr(0,49):decr=item.metaDescription;
                let parsedDate = parseTime(item);
                articlesMeta.push(
                    {
                        id: item._id,
                        title: item.title,
                        descr: decr,
                        publishDate: parsedDate
                    }
                )
            })
            return {...state, articlesMeta, page:payload.page, pagesCount: payload.pagesCount, pending: false}
        case types.GET_ARTICLES_FAILED:
            return {...state, error: 'Cannot load articles'}
        default:
            return state
    }
}

