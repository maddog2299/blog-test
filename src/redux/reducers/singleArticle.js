'use strict';
import types from '../constants';
const initialState = {};
import {parseTime} from '../../helper';

export default function singleArticle (state = initialState, action) {
    let payload = action.payload;
    switch (action.type) {
        case types.GET_SINGLE_SUCCESS:
           let postData = {},
               id = payload._id,
               parsedDate = parseTime(payload),
               title = payload.title,
               comments = payload.comments,
               body = payload.body;
            postData.id = id;
            postData.parsedDate = parsedDate;
            postData.title = title;
            postData.comments = comments;
            postData.body = body;
            return {...state, postData: postData}
        case types.UPDATE_ARTICLE_SUCCESS:
            return {state}
        case types.POST_COMMENT_REPLY_TO:
            return {...state, replyToId: payload}
        case types.GET_SINGLE_FAILED:
            return {...state, error: 'Cannot load single article'}
        case types.POST_COMMENT_FAILED:
            return {...state, error: 'Cannot load comments'}
        case types.UPDATE_ARTICLE_FAILED:
            return {...state, error: 'Cannot update article'}
        case types.CREATE_ARTICLE_FAILED:
            return {...state, error: 'Cannot create new article'}
        default:
            return state
    }
}