'use strict';
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import articles from './articles';
import singleArticle from './singleArticle';

export default combineReducers({
    routing: routerReducer,
    articles,
    singleArticle
})
