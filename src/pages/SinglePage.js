'use strict';

import React from 'react';
import PostView from '../components/single/PostView';
import FormComments from '../components/single/FormComments';
import CommentsList from '../components/single/CommentsList';
import {getArticleByID, postComment, replyTo} from '../redux/actions/index';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

class SinglePage extends React.Component {

    constructor(props){
        super(props);
        this.state={
            postData: {
                id: "",
                comments: []
            }
        }
    }

    componentDidMount() {
       let path = this.props.location.pathname,
           id = path.split('/')[2];
       if(typeof id !== 'undefined'){
           this.props.getArticleByID(id);
       }
       else{
           window.location.href="/error"
       }
    }


    componentWillReceiveProps(nextProps){
        let postData = nextProps.postData
        this.setState({postData});
    }


    render() {
        let postData = this.state.postData;
        return (
            <section className="single-page">
                <PostView
                    postData={postData}
                />
                <CommentsList
                    comments={postData.comments}
                    replyTo={this.props.replyTo}
                />
                <FormComments
                    postComment={this.props.postComment}
                    articleId={postData.id}
                    replyToId={this.props.replyToId}
                    getArticleByID={this.props.getArticleByID}
                />
            </section>
        )
    }
}

function mapStateToProps(state) {
    return {
        postData: state.singleArticle.postData,
        replyToId: state.singleArticle.replyToId
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getArticleByID: bindActionCreators(getArticleByID, dispatch),
        postComment: bindActionCreators(postComment, dispatch),
        replyTo: bindActionCreators(replyTo, dispatch)
    }
}

SinglePage.propTypes = {
    getArticleByID: React.PropTypes.func.isRequired,
    postData: React.PropTypes.object
}


export default connect(mapStateToProps, mapDispatchToProps)(SinglePage)