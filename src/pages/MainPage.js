'use strict'

import React from 'react';
import ArticlePreview from '../components/main/ArticlePreview';
import Pagination from '../components/main/Pagination';
import {bindActionCreators} from 'redux';
import {getArticles} from '../redux/actions/index';
import {connect} from 'react-redux';

class MainPage extends React.Component {

    constructor(props){
        super(props);
        this.state={
            articlesMeta: [],
            page: 1
        }
    }

    componentDidMount(){
        let page  = parseInt(this.props.location.query.page);
        typeof page == 'undefined'||!page ? page = 1 : page;
        this.props.getArticles(page);
        this.setState({page});
    }

    componentWillReceiveProps(nextProps){
        let articlesMeta = nextProps.articlesMeta
        this.setState({articlesMeta})
    }

    render() {
        let preview = this.state.articlesMeta.map((article, i) =>{
            return(
                <ArticlePreview
                    key={i}
                    id={article.id}
                    title={article.title}
                    descr={article.descr}
                    date={article.publishDate}
                />
            )
        });
        return (
        <div className="article-wrapper">
            {preview}
            <Pagination
                activePage={this.state.page}
                pagesCount={this.props.pagesCount}
                getArticles={this.props.getArticles}
            />
        </div>
        );
    }
}

function mapStateToProps(state){
    return {
        articlesMeta: state.articles.articlesMeta,
        error: state.error,
        pagesCount: state.articles.pagesCount,
        page: state.articles.page
    }
}

function mapDispatchToProps(dispatch){
    return {
        getArticles: bindActionCreators(getArticles, dispatch),
    }
}

MainPage.propTypes = {
    getArticles: React.PropTypes.func.isRequired,
    articlesMeta: React.PropTypes.array
}

export default connect(mapStateToProps, mapDispatchToProps)(MainPage)
