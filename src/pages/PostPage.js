'use strict';
import React from 'react';
import TinyMCE from 'react-tinymce';
import {createPost} from '../redux/actions/index';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Link} from 'react-router';

class PostPage extends React.Component {

    constructor(props){
        super(props);
        this.state={
            title: "",
            metaTitle: "",
            metaDescr: "",
            metaKeys: "",
            body: "",
            isFill: false
        }
    }

    handleEditorChange(event) {
        let body = event.target.getContent();
        this.setState({
            body
        })
    }

    handleChangeTitle(event) {
        this.setState({title: event.target.value});
    }

    handleChangeMetaTitle(event) {
        this.setState({metaTitle: event.target.value});
    }

    handleChangeMetaDescr(event) {
        this.setState({metaDescr: event.target.value});
    }

    handleChangeMetaKeys(event) {
        this.setState({metaKeys: event.target.value});
    }

    handlePostCreate(event, data){
        event.preventDefault();
        this.props.createPost(data);
        this.setState({
            isFill: true
        })
    }

    render(){
        let data = {
            title: this.state.title,
            status: 'active',
            metaTitle: this.state.metaTitle,
            metaDescription: this.state.metaDescr,
            metaKeywords: this.state.metaKeys,
            body: this.state.body
        }
        return(
            <section className="post-section">
                { !this.state.isFill
                    ?
                    <div id="form" name="form" className="form-wrapper">
                        <form onSubmit={(event)=>this.handlePostCreate(event, data)}>
                            <div className="input-wrapper">
                                <input placeholder="Title" type="text" className="title" name="title" ref="title"
                                       required value={this.state.title} onChange={this.handleChangeTitle.bind(this)}/>
                                <input placeholder="Meta Title" ref="mtitle" required type="text" className="mtitle"
                                       name="mtitle" value={this.state.metaTitle}
                                       onChange={this.handleChangeMetaTitle.bind(this)}/>
                                <input placeholder="Meta Description" ref="mdescr" required type="text"
                                       className="mdescr" name="mdescr" value={this.state.metaDescr}
                                       onChange={this.handleChangeMetaDescr.bind(this)}/>
                                <input placeholder="Meta Keywords" ref="mkeys" required type="text" className="mkeys"
                                       name="mkeys" value={this.state.metaKeys}
                                       onChange={this.handleChangeMetaKeys.bind(this)}/>
                            </div>
                            <TinyMCE
                                content=""
                                config={{
                                    plugins: 'autolink link image lists preview',
                                    toolbar: 'fontsizeselect',
                                    fontsize_formats: "8px 12px 16px 18px 22px 24px"
                                }}
                                onChange={this.handleEditorChange.bind(this)}
                            />
                            <div className="submit-wrapper">
                                <input type="submit" value="Submit Post"/>
                            </div>
                        </form>
                    </div>
                    :
                    <div>
                        <p>Your post was created!</p>
                        <Link to="/">Go to Main Page</Link>
                    </div>
                }
            </section>
        )
    }
}


function mapStateToProps(state) {
    return state
}

function mapDispatchToProps(dispatch) {
    return {
        createPost: bindActionCreators(createPost, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostPage)