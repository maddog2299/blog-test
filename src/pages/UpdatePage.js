'use strict';
import React from 'react';
import TinyMCE from 'react-tinymce';
import {getArticleByID, changeContent} from '../redux/actions/index';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {hashHistory} from 'react-router';

class UpdatePage extends React.Component {

    constructor(props){
        super(props);
        this.state={
            postData: {
                body: ""
            }
        }
    }

    handleEditorChange(event) {
        let body = event.target.getContent();
        this.setState({
            body
        })
    }

    handlePostUpdate(event, data){
        event.preventDefault();
        let body = this.state.body;
        if(body.length<1){
            body = data.body;
        }
        this.props.changeContent(data.id, body);
        hashHistory.goBack();
    }

    componentDidMount() {
        let path = this.props.location.pathname,
            id = path.split('/')[3];
        if(typeof id !== 'undefined'){
            this.props.getArticleByID(id);
        }
        else{
            window.location.href="/error"
        }
    }

    componentWillReceiveProps(nextProps){
        let postData = nextProps.postData;
        this.setState({postData});
    }

    render(){
        let data = this.state.postData;
        return(
            <section className="update-section">
                { !!data.body.length > 0
                    ?
                <div>
                    <h1>{data.title}</h1>
                    <TinyMCE
                        content={data.body}
                        config={{
                            plugins: 'autolink link image lists preview',
                            toolbar: 'fontsizeselect',
                            fontsize_formats: "8px 12px 16px 18px 22px 24px"
                        }}
                        onChange={this.handleEditorChange.bind(this)}
                    />
                    <div className="update-link-wrapper">
                        <Link onClick={(event)=>this.handlePostUpdate(event, data)}>Save Post</Link>
                    </div>
                </div>
                    : <p>Loading...</p>
                }
            </section>
        )
    }
}

function mapStateToProps(state) {
    return {
        postData: state.singleArticle.postData,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getArticleByID: bindActionCreators(getArticleByID, dispatch),
        changeContent: bindActionCreators(changeContent, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdatePage)