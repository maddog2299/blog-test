'use strict'
require('../styles/main.scss');

import React from 'react';
import 'jquery';
import 'bootstrap-loader';
import Header from './HeaderComponent';
import SideBar from './SideBarComponent';
import {connect} from 'react-redux';

class Main extends React.Component {

    constructor(props){
        super(props);
        this.state={
            pending: false
        }
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            pending: nextProps.pending
        })
        if (this.props.error){
            alert(this.props.store.error);
        }
    }

    render() {
        return (
            <div>
                <div>
                    {this.state.pending &&
                    <div className="container">
                        <div className="flex-loading-wrapper">
                            <h1>Loading..</h1>
                        </div>
                    </div>
                    }
                </div>
                    <div>
                        <Header />
                        <section className="content-section">
                            <div className="container">
                                <div className="col-md-9">
                                    {this.props.children}
                                </div>
                                <div className="col-md-3">
                                    <SideBar />
                                </div>
                            </div>
                         </section>
                    </div>
            </div>
    );
  }
}

function mapStateToProps(state) {
    return {
        pending: state.articles.pending,
        error: state.articles.error
    }
}

export default connect(mapStateToProps)(Main)
