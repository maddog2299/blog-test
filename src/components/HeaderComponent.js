'use strict'
import React from 'react';

export default class HeaderComponent extends React.Component {
    render() {
        return (
            <header>
                <div className="container">
                    <a href="/">Blog</a>
                </div>
            </header>
        );
    }
}

