'use strict'

import React from 'react';
import {Link} from 'react-router';

export default class ArticlePreview extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        let date = this.props.date;
        return (
            <article className="preview-article-wrapper">
                <div className="flex-article-preview">
                    <div className="preview-photo">
                        <div className="image-article-wrapper">
                            <div className="publish-date-wrapper">
                                <div className="publish-date">
                                    <h3>{date.number}</h3>
                                </div>
                                <div className="publish-day">
                                    <p>{date.day}</p>
                                    <span>{`${date.month}, ${date.year}`}</span>
                                </div>
                            </div>
                            <img src="/src/images/test-img.png" alt=""/>
                        </div>
                    </div>
                    <div className="preview-meta">
                        <div className="content-article-wrapper">
                            <h3>
                                {this.props.title}
                            </h3>
                            <p>
                                {this.props.descr}
                            </p>
                            <Link to={`/post/${this.props.id}`}>continue reading</Link>
                        </div>
                    </div>
                </div>
            </article>
        );
    }
}

ArticlePreview.propTypes = {
    title: React.PropTypes.string.isRequired,
    descr: React.PropTypes.string,
    date: React.PropTypes.object.isRequired
}