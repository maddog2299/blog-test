/**
 * Created by Artyom on 14.02.2017.
 */
import React from 'react';
import ReactPaginate from 'react-paginate';

export default class Pagination extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            pagesCount:0,
            activePage: 0,
            getArticles:() => null
        }
    }

    componentWillMount(){
        this.setState({activePage: this.props.activePage})
    }

    componentWillReceiveProps(nextProps){
        let activePage = nextProps.activePage,
            pagesCount = nextProps.pagesCount,
            getArticles = nextProps.getArticles;
        this.setState({activePage, pagesCount, getArticles})
    }

    pageChange(arg){
        let activePage = arg.selected + 1;
        this.state.getArticles(activePage)
    }

    render(){
        let page = this.state.activePage - 1;
        return(
            <div className="pagination-wrapper">
                <ReactPaginate
                    pageCount={this.state.pagesCount}
                    previousLabel={<i className="fa fa-angle-left" aria-hidden="true"></i>}
                    nextLabel={<i className="fa fa-angle-right" aria-hidden="true"></i>}
                    onPageChange={(arg) => this.pageChange(arg)}
                    pageClassName={"page-link"}
                    activeClassName={"active-page"}
                    marginPagesDisplayed={1}
                    pageRangeDisplayed={4}
                    initialPage={page}
                    forcePage={page}
                />
            </div>
        )
    }
}