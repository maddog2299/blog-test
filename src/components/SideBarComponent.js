'use strict'
import React from 'react';
import Collapse, { Panel } from 'rc-collapse';
import {Link} from 'react-router';
import 'rc-collapse/assets/index.css';

export default class SideBarComponent extends React.Component {

    render() {
        let calendar =
            <div className="column-header">
                <i className="fa fa-calendar" aria-hidden="true"></i>
                <span>2016</span>
                <i className="fa fa-angle-down arrow-menu" aria-hidden="true"></i>
            </div>
        return (
            <div className="sidebar-wrapper">
               <div className="contact-wrapper">
                   <a href="#">Contact us today for More information. <i></i> </a>
               </div>
                <div className="popular-wrapper">
                    <div className="column-header">
                        <i className="fa fa-star" aria-hidden="true"></i>
                        <span>Popular</span>
                    </div>
                    <div className="popular-content">
                        <ul>
                            <li>
                                <h3>Articles of incorporations</h3>
                                <p>A document that establis by other names ...</p>
                            </li>
                            <li>
                                <h3>Articles of incorporations</h3>
                                <p>A document that establis by other names ...</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="archive-wrapper">
                    <div className="column-header">
                        <i className="fa fa-archive" aria-hidden="true"></i>
                        <span>Archive</span>
                    </div>
                    <Collapse showArrow={false} accordion={true}>
                        <Panel className="panel-wrapper" header={calendar}>
                            <ul>
                                <li>
                                    <a href="">
                                        <span>March</span>
                                        <span>(0)</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span>May</span>
                                        <span>(0)</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <span>July</span>
                                        <span>(0)</span>
                                    </a>
                                </li>
                            </ul>
                        </Panel>
                    </Collapse>
                    <div className="update-link-wrapper">
                        <Link to = "/post/create/new/">New Post</Link>
                    </div>
                </div>
            </div>
        );
    }
}
