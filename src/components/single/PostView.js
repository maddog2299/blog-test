'use strict';
import React from 'react';
import {Link} from 'react-router';

export default class PostView extends React.Component {

    constructor(props){
        super(props);
        this.state={
            id: '',
            date:{},
            title:'',
            body:''
        }
    }

    componentWillReceiveProps(nextProps){
        let data = nextProps.postData,
            date = data.parsedDate,
            id  = data.id,
            title = data.title,
            body = data.body;
        this.setState({
            date, id, title, body
        })

    }

    render() {
        let date = this.state.date;
        return (
            <section className="post-view">
                { !!this.state.body
                    ?
                <article className="preview-article-wrapper full-article">
                    <div className="image-article-wrapper">
                        <div className="publish-date-wrapper">
                            <div className="publish-date">
                                <h3>{date.number}</h3>
                            </div>
                            <div className="publish-day">
                                <p>{date.day}</p>
                                <span>{`${date.month}, ${date.year}`}</span>
                            </div>
                        </div>
                        <img src="/src/images/test-img-full.png" alt=""/>
                    </div>
                    <div className="text-post-content">
                        <h1>{this.state.title}</h1>
                        <div dangerouslySetInnerHTML={{__html: this.state.body}} />
                    </div>
                    <div className="update-link-wrapper">
                        <Link to={`/post/update/${this.state.id}`}>
                            Update Post
                        </Link>
                    </div>
                </article>
                    : <p id="content">Loading...</p>
                }
            </section>
        )
    }
}