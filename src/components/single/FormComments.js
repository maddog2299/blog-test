'use strict';

import React from 'react';
export default class FormComments extends React.Component {

    constructor(props){
        super(props);
        this.state={
            name: "",
            email: "",
            comment: "",
            replyToId: {}
        }
    }

    handleChangeName(event){
        this.setState({name: event.target.value});
    }

    handleChangeEmail(event){
        this.setState({email: event.target.value});
    }

    handleChangeComment(event){
        this.setState({comment: event.target.value});
    }

    handleSubmit(event){
        event.preventDefault();
        let articleId = this.props.articleId;

        let data = {
            articleId: articleId,
            text: this.state.comment,
            author:  this.state.email,
            authorName: this.state.name
        }

        if(this.state.replyToId){
            data.parentId = this.state.replyToId.id
        }

        this.props.postComment(data, articleId);

        this.setState({
            name: "",
            email: "",
            comment: ""
        })
    }

    componentWillReceiveProps(nextProps){
        let replyToId = nextProps.replyToId;
        if(replyToId){
            this.setState({
                replyToId: replyToId,
                comment: replyToId.author+','
            })
        }
    }

    render() {
        return(
            <div id = "form" name = "form" className="form-wrapper">
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div className="input-wrapper">
                        <input placeholder="Name" type="text" className="name" name="name" ref="name" required value={this.state.name} onChange={this.handleChangeName.bind(this)}  />
                        <input placeholder="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" ref="email" required type="text" className="email" name="email" value={this.state.email} onChange={this.handleChangeEmail.bind(this)} />
                    </div>
                    <textarea ref="comment" name="form-text" id="form-text" cols="30" rows="10" value={this.state.comment} onChange={this.handleChangeComment.bind(this)}></textarea>
                    <div className="submit-wrapper">
                        <input type="submit" value="Submit Comment"/>
                    </div>
                </form>
            </div>
        )
    }
}