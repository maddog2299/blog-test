'use strict';
import React from 'react';

export default class CommentsList extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            comments:[],
            commentsTitle: "",
            replyTo: ""
        }
    }

    componentWillReceiveProps(nextProps){
        let commentsTitle = "There is no comments yet!",
            replyTo = nextProps.replyTo,
            comments = nextProps.comments;
        if(comments.length > 0){
            commentsTitle = "Comments:"
        }

        this.setState({comments, commentsTitle, replyTo})
    }

    handleClick(event, id, author){
        event.preventDefault();
        this.state.replyTo(id ,author);
        window.scrollBy(0, document.body.offsetHeight);
    }

    render(){
        let comments = this.state.comments.map((item, i) => {
            let replies = item.replies.map((reply, key) => {
                return(
                    <li key={key}>
                        <p className="reply-name">
                            {reply.authorName||item.author}
                        </p>
                        <p className="reply-email">
                            {reply.created}
                        </p>
                        <p className="reply-message">
                            {reply.text}
                        </p>
                    </li>
                )
            })
            return(
                <li key={i}>
                    <p className="user-name">
                        {item.authorName||item.author}
                    </p>
                    <p className="user-email">
                        {item.created}
                    </p>
                    <p className="user-message">
                        {item.text}
                    </p>
                    <span className="reply">
                        Reply to comment # <a href="#form" onClick={(event) => this.handleClick(event, item._id, item.authorName||item.author)}>{item._id}</a>
                    </span>
                    {replies.length > 0 &&
                    <ul className="reply-list">
                        {replies}
                    </ul>
                    }
                </li>
            )
        })
        return(
            <div className="comments-list">
                <h1>{this.state.commentsTitle}</h1>
                <ul className="main-comments">
                    {comments}
                </ul>
            </div>
        )
    }
}