'use strict'

import React from 'react';
import {render} from 'react-dom';
import {Router, Route} from 'react-router';
import Main from './components/MainComponent';
import {syncHistoryWithStore} from 'react-router-redux';
import {Provider} from 'react-redux';
import store from './redux/store';
import { hashHistory } from 'react-router';
import MainPage from './pages/MainPage';
import SinglePage from './pages/SinglePage';
import ErrorPage from './pages/ErrorPage';
import UpdatePage from './pages/UpdatePage';
import PostPage from './pages/PostPage'

const history = syncHistoryWithStore(hashHistory, store);

render((
    <Provider store={store}>
        <Router history={history}>
            <Route name="home" component={Main}>
                <Route path="/(:pageNumber)" component={MainPage} />
                <Route path="/post/:id" component={SinglePage} />
                <Route path="/post/update/:id" component={UpdatePage} />
                <Route path="/post/create/new" component={PostPage} />
                <Route path="*" component={ErrorPage} />
             </Route>
        </Router>
  </Provider>
), document.getElementById('main'));