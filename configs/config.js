'use strict'
module.exports = {
    apiUrl: 'http://api.blog.dev.singree.com',
    publicPath: '/public/',
    host: 'localhost',
    port: 8085,
    pageLimit: 3
};